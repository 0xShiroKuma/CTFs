# Qrcode - MISC

## Description :

Voici la description fournit pour ce challenge :

![Description](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/1.PNG)

## Recon

Une commande netcat nous est indiquée pour nous connecter sur le port tcp 3001. Allons tout d'abord jeter un coup d'oeil sur celui-ci :

![netcat](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/1.jpg)

Tout est dit ici : "I will send you a PNG image compressed by zlib encoded in base64 that contains 64 encoded numbers. The expected answer is the sum of all the numbers (in decimal)"

Répondons "Y" afin de voir ce qui se passe ensuite :

![base64](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/2.jpg)

On voit bien des données encodées en base64 tel qu'indiqué par le serveur. De plus, on remarque rapidement l'apparition du message "Time is up!", on va donc devoir automatiser la résolution du challenge pour être dans les temps.
Pour cela on va commencer par "résoudre" le challenge à la main avant l'automatisation.

On enregistre les données en base64 dans un fichier, puis on les décode :

![base64](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/3.jpg)

Enfin on décompresse les données avec zlib :

![zlib](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/4.jpg)


Regardons à quoi l'image ressemble :

![netcat](https://framagit.org/0xShiroKuma/CTFs/raw/master/write-ups-2019/Quals%20ECSC%20FR%202019/MISC/qrcode/img/data.png)

On voit tout de suite 16 qrcodes, si l'on en décode plusieurs on obtient à chaque fois un nombre, exemple : 4280462110. On a donc nos nombre à additionner. Il ne nous reste plus qu'à automatiser ce processus :)

## Résolution 

Afin de résoudre se challenge nous devons automatiser la réception des données, leurs traitements, ainsi que l'envoie de la réponse au serveur distant afin d'être le plus rapide possible.
Résumons les étapes que notre script d'automatisation va devoir effectuer :

1.  Réception des données encodées en base64 et les décoder
2.  Décompresser les données
3.  Décoder chaque qrcode
4.  Faire la somme des valeurs de chaque qrcode
5.  Envoyer cette somme au serveur distant

Pour ma part j'ai fait le choix d'utiliser python, mais la plupart des langages peuvent être choisis.
Afin de simplifier la communication avec le serveur distant on va utiliser [pwntools](https://github.com/Gallopsled/pwntools) :

On se connecte : 
```
r = remote('challenges.ecsc-teamfrance.fr' , 3001)
```

On envoie la réponse 'Y' au serveur après qu'il nous pose la première question pour qu'il nous envoi les données par la suite:
```
tmp = r.recvuntil('>> ', drop=True)
r.send('Y\n')
```

Le serveur nous envoi bien des données en base64. Pour récupérer ces données on utilise la fonction recvuntil() qui va recevoir jusqu'à la chaîne de caractère "What is you answer?", positionnée juste après les données utiles. Sachant que grâce au paramètre drop cette chaîne ne va pas être contenu dans la variable b64.
```
b64 = r.recvuntil('What is you answer?', drop=True)
```

Nous avons maintenant les informations encodées en base64, il nous faut ensuite les décoder, puis les décompresser avec zlib :
```
zipf = base64.b64decode(b64)
img = zlib.decompress(zipf)
```

A cette étape du processus nous sommes en possession des données brut représentant une image, on va donc l'enregistrer puis la traiter :
```
f = open("img.tmp", "w")
f.write(img)
f.close()
```

Etant donné que qrtools ne peut décoder qu'une image contenant un seul qrcode nous allons devoir les isoler. Pour cela nous allons utiliser le module PIL afin de recadrer 16 fois l'image, pour avoir une image par qrcode. De plus, grâce à la disposition symétrique des qrcodes sur l'image, il nous sera très facile d'automatiser le recadrage avec une boucle.

Première étape, on ouvre l'image avec PIL:
```
im = Image.open("img.tmp")
```

On va ensuite faire deux boucles imbriqué allant de 0 à 7. La première pour nos 8 colones et la seconde pour nos 8 lignes de qrcodes.
On recadre ligne par ligne chaque qrcode de gauche à droite grâce à la fonction crop() de notre objet im. La valeur que nous utilisons pour le décalage entre chaque qrcode n'est qu'approximative (290), ce qui va engendrer un léger décalage du positionnement du qrcode dans nos images, mais sans empêcher notre script de décoder les qrcode.
De plus, la valeur à utiliser pour recadrer à droite sera supérieur à taille de l'image sur les derniers qrcode de chaque ligne. De la même manière, la valeur concernant le bas de l'image sera invalide pour tous les qrcode de la dernière ligne. Pour palier à cela on va tout simplement définir leurs valeurs à 2320 (dimenssion de l'image) si les valeurs right ou bot sont supérieurs à celle-ci :

```
for y in range(0, 8):
    for i in range(0, 8):
        left = i*290
        right = (i+1)*290
        top = y*290
        bot = (y+1)*290

        if right > 2320:
            right = 2320
        if bot > 2320:
            bot = 2320

        imgCropped = im.crop((left, top, right, bot))
        currentImgName = str(i)+"x"+str(y)+".png"
        imgCropped.save(currentImgName)

```

Il ne nous manque plus qu'à décoder chaque qrcode et faire la somme des valeurs contenues dans chacun d'eux. Dans la même boucle on va donc décoder chaque image re-cadré et traiter la valeur récupérée :

```
qr = qrtools.QR()
        qr.decode(currentImgName)
        sum += int(qr.data)
```

Une fois sortie de notre boucle la dernière étape va être d'envoyer notre somme calculé au serveur distant :

```
r.send(str(sum)+"\n")
r.interactive()
```

Voici le code complet du script :

```
from pwn import *
import base64
import zlib
from PIL import Image
import qrtools

r = remote('challenges.ecsc-teamfrance.fr' , 3001)
tmp = r.recvuntil('>> ', drop=True)
r.send('Y\n')
b64 = r.recvuntil('What is you answer?', drop=True)
zipf = base64.b64decode(b64)
img = zlib.decompress(zipf)
f = open("img.tmp", "w")
f.write(img)
f.close()
im = Image.open("img.tmp")
sum = 0

for y in range(0, 8):
    for i in range(0, 8):
        left = i*290
        right = (i+1)*290
        top = y*290
        bot = (y+1)*290

        if right > 2320:
            right = 2320
        if bot > 2320:
            bot = 2320

        imgCropped = im.crop((left, top, right, bot))
        currentImgName = str(i)+"x"+str(y)+".png"
        imgCropped.save(currentImgName)

        qr = qrtools.QR()
        qr.decode(currentImgName)
        sum += int(qr.data)

print(sum)
r.send(str(sum)+"\n")
r.interactive()

```

Done !

