from pwn import *
import base64
import zlib
from PIL import Image
import qrtools

r = remote('challenges.ecsc-teamfrance.fr' , 3001)
tmp = r.recvuntil('>> ', drop=True)
r.send('Y\n')
b64 = r.recvuntil('What is you answer?', drop=True)
zipf = base64.b64decode(b64)
img = zlib.decompress(zipf)
f = open("img.tmp", "w")
f.write(img)
f.close()
im = Image.open("img.tmp")
sum = 0

width, height = im.size
for y in range(0, 8):
    for i in range(0, 8):
        left = i*290
        right = (i+1)*290
        top = y*290
        bot = (y+1)*290

        if right > 2320:
            right = 2320
        if bot > 2320:
            bot = 2320

        imgCropped = im.crop((left, top, right, bot))
        currentImgName = str(i)+"x"+str(y)+".png"
        imgCropped.save(currentImgName)

        qr = qrtools.QR()
        qr.decode(currentImgName)
        sum += int(qr.data)

print(sum)
r.send(str(sum)+"\n")
r.interactive()
